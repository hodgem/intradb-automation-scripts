import com.google.common.collect.Maps
import com.google.common.io.Files
import java.lang.Exception
import java.lang.ProcessBuilder
import java.net.InetAddress
import java.net.UnknownHostException
import org.apache.commons.io.FileUtils
import org.nrg.xdat.XDAT
import org.nrg.xdat.entities.AliasToken
import org.nrg.xdat.services.AliasTokenService


def localhost = null;
try {
         localhost = InetAddress.getLocalHost();
} catch (UnknownHostException unknownHostException) { 
         println  "ERROR:  Unknown Host (" + unknownHostException.getClass().toString() + "): " +
            unknownHostException.getMessage() 
         throw unknownHostException
         
}

def token = XDAT.getContextService().getBean(AliasTokenService.class).issueTokenForUser(user)

def executeOnShell(String command) {
  return executeOnShell(command, new File(System.properties.'user.dir'))
}
 
def executeOnShell(String command, File workingDir) {
  def process = new ProcessBuilder(addShellPrefix(command))
                                    .directory(workingDir)
                                    .redirectErrorStream(true) 
                                    .start()
  process.inputStream.eachLine {
      println it
      //System.out.println(it)
  }
  process.waitFor();
  return process.exitValue()
}
 
def addShellPrefix(String command) {
  commandArray = new String[3]
  commandArray[0] = "sh"
  commandArray[1] = "-c"
  commandArray[2] = command
  return commandArray
}

def tempDir = Files.createTempDir();

try {
    def cmd = "/nrgpackages/tools.release/intradb/sanity_checks/callSanityChecks.sh --host https://" +
        localhost.getHostName() + "/ --user " + token.getAlias() + " --pw " + token.getSecret() + " --project " +
        projectId + " --subject " + subjectLbl + " --exp " + experimentLbl + " --create-report"
    executeOnShell(cmd, tempDir)
} catch (e) {
    println "EXCEPTION:  " + e
    //System.out.println("EXCEPTION:  " + e)
}

FileUtils.deleteDirectory(tempDir);
